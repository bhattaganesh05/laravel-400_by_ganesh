<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{$title ?? ''}}</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-lg-8  m-auto">
                    <div class="card mt-2">
                        @include('../flash_message')
                        <div class="card-header ">
                            <h5 class="font-weight-bold">{{$title ?? 'Blog Form'}}</h5>
                        </div>
                        <div class="card-body">
                            <form action="{{ isset($edit_blog_data)?route('blog.update',$edit_blog_data->id):route('blog.store') }}" method="post" enctype = "multipart/form-data" class = "form">
                                {{--                        @csrf--}}
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                @if(isset($edit_blog_data))
                                @method('put')
                                @endif
                                <div class="form-group row">
                                    <div class="col-3 text-right">
                                        <label for="blog_title" class=" col-form-label ">Blog Title</label>
                                    </div>
                                    <div class="col-8">
                                        <input type="text" class="form-control " value="{{@$edit_blog_data->blog_title ?? old('blog_title')}}" required name="blog_title" id="blog_title" placeholder="Enter title for blog">
                                        @error('blog_title')
                                        <span class="alert-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-3 text-right">
                                        <label for="blog_summary" class=" col-form-label ">Summary</label>
                                    </div>
                                    <div class="col-8">
                                        <textarea style="resize: none;" required  class="form-control"  name="blog_summary" id="blog_summary" placeholder="Enter summary for blog">{{@$edit_blog_data->blog_summary ?? old('blog_summary')}}</textarea>
                                        @error('blog_summary')
                                        <span class="alert-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-3 text-right">
                                        <label for="blog_description" class=" col-form-label ">Description</label>
                                    </div>
                                    <div class="col-8">
                                        <textarea style="resize: none;" class="form-control " name="blog_description" id="blog_description" placeholder="Enter Description for blog">{{@$edit_blog_data->blog_description ?? old('blog_description')}}</textarea>
                                        @error('blog_description')
                                        <span class="alert-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-3 text-right">
                                        <label for="category_id" class=" col-form-label ">Blog Category</label>
                                    </div>
                                    <div class="col-8">
                                        <select  class="form-control " name="category_id" id="category_id" >
                                            <option value="" selected disabled>__Select-Category__</option>
                                            @if($all_cat->count())
                                            @foreach($all_cat as $cat)
                                            <option value="{{$cat->id}}" {{(@$edit_blog_data->category_id == $cat->id) ? 'selected': '' }}>{{$cat->category_title}}</option>
                                            @endforeach
                                            @endif;
                                        </select>
                                        @error('category_id')
                                        <span class="alert-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-3 text-right">
                                        <label for="blog_status" class=" col-form-label ">Blog Status</label>
                                    </div>
                                    <div class="col-8">
                                        <select class="form-control " name="blog_status" id="blog_status" >
                                            <option value="active" {{@$edit_blog_data->blog_status == 'active' ? 'selected' : '' }}>Published</option>
                                            <option value="inactive" {{@$edit_blog_data->blog_status == 'inactive' ? 'selected' : '' }}>Un-Published</option>
                                        </select>
                                        @error('blog_status')
                                        <span class="alert-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-3 text-right">
                                        <label for="blog_image" class=" col-form-label ">Image</label>
                                    </div>
                                    <div class="col-4">
                                        <input type="file" class="form-control-file " onchange="readURL(this,'thumb')" name="blog_image" id="blog_image" accept="image/*">
                                    </div>
                                    @error('blog_image')
                                    <span class="alert-danger">{{ $message }}</span>
                                    @enderror
                                    <div class="col-3">
                                        <img src="{{\Illuminate\Support\Facades\Storage::url(@$edit_blog_data->blog_image) ?? '' }}" alt="" class="img img-fluid" id="thumb" height="200px" width="200px">
                                    </div>
                                </div>
                                <input type="hidden" name="folder_name" value="{{@$edit_blog_data->blog_image}}">
                                <div class="form-group row">
                                    <div class="offset-3 col-8 ">
                                        <button type="submit" class="btn btn-success btn-block font-weight-bold">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{route('blog.index')}}" style="text-decoration: none;"><i class="fa fa-lef-arrow"></i> Go Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src = "{{asset('js/app.js')}}"></script>
        <script>
        function readURL(input,image_id){
        if(input.files && input.files[0]){
        var reader  = new FileReader();
        reader.onload = function (e){
        $('#'+image_id)
        .attr('src',e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
        }
        }
        </script>
    </body>
</html>
