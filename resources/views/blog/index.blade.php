@php
use Illuminate\Support\Facades\Storage;
@endphp
@extends('../layouts.blog')
@section('header_content')
    <link rel="stylesheet" href="{{asset('plugins/datatables/datatables.min.css')}}">
@endsection
@section('container_class','container-fluid')
@section('main_content')
                <div class="col-4">
                    <h4 class="m-2 font-weight-bold">Blog List</h4>
                </div>
                <div class="  offset-4 col-4 text-right ">
                    <a href="{{route('blog.create')}}" class="btn btn-success m-2">Add Blog</a>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @include('../flash_message')
                    <table class="table table-sm  table-hover table-bordered text-center table-striped">
                        <thead class="thead-dark">
                            <th>S.N.</th>
                            <th>Title</th>
                            <th>Summary</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if($blog_data->count())
                            @foreach($blog_data  as  $value)
                            <tr>
                                <td>{{$value->id}}</td>
                                <td>{{$value->blog_title}}</td>
                                <td>{{$value->blog_summary}}</td>
                                <td>{{$value->blog_description}}</td>
                                <td>{{$value->category_info->category_title}}</td>
                                <td>{{$value->blog_status}}</td>
                                <td>
                                    @if(($value->blog_image))
                                    <img src="{{Storage::url($value->blog_image)}} " alt="" class="img-fluid img-thumbnail" width="100" height="100">
                                    @endif
                                </td>
                                <td>
                                    <form action="{{ route('blog.destroy',$value->id) }}" method="POST"  onsubmit= "return confirm('Are you sure, you want to this blog?')">
                                        <a href="{{route('blog.edit',$value->id)}}" class="" title="Edit this blog"><i class=" fa fa-pen"></i></a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class=" btn btn-link" style="border: none;"><a href="javascript:;" title="Delete this blog"><i class="fa fa-trash"></i></a></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                    {{--            <span>{{$blog_data->links()}}</span>--}}
                </div>
            </div>
@endsection
@section('footer_script_src')
<script src = "{{asset('js/app.js')}}"></script>
<script src = "{{asset('plugins/datatables/datatables.min.js')}}"></script>
@endsection
@section('script_js')
$('.table').DataTable();
@endsection
    </body>
</html>
