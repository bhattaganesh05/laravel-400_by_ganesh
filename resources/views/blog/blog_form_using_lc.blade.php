@extends('../layouts.blog')
@section('container_class','container')
@section('main_content')
        <div class="col-md-8 col-sm-12 col-lg-8  m-auto">
            <div class="card mt-2">
                @include('../flash_message')
                <div class="card-header ">
                    <h5 class="font-weight-bold">{{$title ?? 'Blog Form'}}</h5>
                </div>
                <div class="card-body">
                    {{Form::open( ['url'=> (isset($edit_blog_data)?route('blog.update',$edit_blog_data->id):route('blog.store')),'files'=>true],['class'=>'form']) }}
                        @if(isset($edit_blog_data))
                            @method('put')
                        @endif
                        <div class="form-group row">
                            <div class="col-3 text-right">
                                {{Form::label('blog_title','Blog Title',['class'=>'col-form-label'])}}
                            </div>
                            <div class="col-8">
                                {{Form::text('blog_title',@$edit_blog_data->blog_title ?? old('blog_title'),['class' => 'form-control','id'=>'blog_title','required' => true,'placeholder' => 'Enter title for blog'] )}}
                                @error('blog_title')
                                <span class="alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 text-right">
                                {{Form::label('blog_summary','Summary',['class'=>'col-form-label'])}}
                            </div>
                            <div class="col-8">
                                {{Form::textarea('blog_summary',@$edit_blog_data->blog_summary ?? old('blog_summary'),['class' => 'form-control','id'=>'blog_summary','required' => true,'placeholder' => 'Enter summary for blog','rows' => 2,'style' => 'resize: none'] )}}
                                @error('blog_summary')
                                <span class="alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 text-right">
                                {{Form::label('blog_description','Description',['class'=>'col-form-label'])}}
                            </div>
                            <div class="col-8">
                                {{Form::textarea('blog_description',@$edit_blog_data->blog_description ?? old('blog_description'),['class' => 'form-control','id'=>'blog_description','required' => true,'placeholder' => 'Enter description for blog','rows' => 2,'style' => 'resize: none'] )}}@error('blog_description')
                                <span class="alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 text-right">
                                <label for="category_id" class=" col-form-label ">Blog Category</label>
                            </div>
                            <div class="col-8">
                                {{Form::select('category_id', $all_cat, @$edit_blog_data->category_id, ['placeholder' => '__Select-Category__','required' => true, 'id' => 'category_id' ,'class' => 'form-control'])}}
                                @error('category_id')
                                <span class="alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 text-right">
                                <label for="blog_status" class=" col-form-label ">Blog Status</label>
                            </div>
                            <div class="col-8">
                                {{Form::select('blog_status', ['active' => 'Published','inactive' => 'Un-published'], @$edit_blog_data->blog_status, ['required' => true, 'id' => 'blog_status' ,'class' => 'form-control'])}}
                                @error('blog_status')
                                <span class="alert-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3 text-right">
                                <label for="blog_image" class=" col-form-label ">Image</label>
                            </div>
                            <div class="col-4">
                                <input type="file" class="form-control-file " onchange="readURL(this,'thumb')" name="blog_image" id="blog_image" accept="image/*">
{{--                                {{Form::file('blog_image',['accept' => 'image/*','id'=>'blog_image','class'=>'form-control-file','onchange'=>"readURL(this,'thumb')"])}}--}}
                            </div>
                            @error('blog_image')
                            <span class="alert-danger">{{ $message }}</span>
                            @enderror
                            <div class="col-3">
                                @if(@$edit_blog_data->blog_image)
                                    <img src="{{\Illuminate\Support\Facades\Storage::url(@$edit_blog_data->blog_image)}}" alt="" class="img-fluid" id="thumb">
                                @else
                                    <img src="" alt="" class="img img-fluid" id="thumb" >
                                @endif
                            </div>
                        </div>
                        {{Form::hidden('folder_name',@$edit_blog_data->blog_image)}}
                        <div class="form-group row">
                            <div class="offset-3 col-8 ">
                                {{Form::button('Submit',['type' => 'submit','class' => 'btn btn-success btn-block font-weight-bold'])}}
                            </div>
                        </div>
                   {{Form::close()}}
                </div>
                <div class="card-footer text-center">
                    <a href="{{route('blog.index')}}" style="text-decoration: none;"><i class="fa fa-lef-arrow"></i> Go Back</a>
                </div>
            </div>
        </div>
@endsection
@section('script_js')
    function readURL(input,image_id){
        if(input.files && input.files[0]){
            var reader  = new FileReader();
            reader.onload = function (e){
                $('#'+image_id)
                    .attr('src',e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
@endsection
</body>
</html>
