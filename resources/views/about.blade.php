<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{$title ?? ''}}</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src = "{{asset('js/app.js')}}"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-4">
                    <img src="{{asset('images/1_(1).jpg')}}" alt="" class="img-fluid ">
                </div>
                <div class="col-4">
                    <img src="{{asset('images/1_(14).jpg')}}" alt="" class="img-fluid ">
                </div>
                <div class="col-4">
                    <img src="{{asset('images/registration-form.jpg')}}" alt="" class="img-fluid ">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table class="table  table-hover table-bordered text-center table-striped">
                        <thead class="thead-dark">
                            <th>S.N.</th>
                            <th>User Name</th>
                            <th>Email Address</th>
                            <th>Mobile Number</th>
                            <th>Address</th>
                        </thead>
                        <tbody>
                            @if($user_data)
                            @foreach($user_data  as $key => $value)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$value['name']}}</td>
                                <td>{{$value['email']}}</td>
                                <td>{{$value['phone']}}</td>
                                <td>{{$value['address']}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>