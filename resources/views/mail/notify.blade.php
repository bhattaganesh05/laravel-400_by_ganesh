<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog creation notification</title>
</head>
<body>
<p>Dear {{$user_name,}}</p>
<p>A blog with title {{$data->blog_title}} has been published. please click the link below to view the detail.</p>\
<a href="{{route('blog.show',$data->id)}}">{{route('blog.show',$data->id)}}</a>
Regards,<br>
System admin <br>
<p>Please do not reply to this email</p>
</body>
</html>
