<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title ?? ''}}</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    @yield('header_content')
    <style>
        @yield('style_css')
    </style>
</head>
<body>
<div class="@yield('container_class')">
    <div class="row">
        @yield('main_content')
    </div>
</div>
<script src = "{{asset('js/app.js')}}"></script>
@yield('footer_script_src')
<script>
    @yield('script_js')
</script>
</body>
</html>
