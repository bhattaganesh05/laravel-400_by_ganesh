<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->string("blog_title");
            $table->text("blog_summary");
            $table->longText("blog_description")->nullable();
            $table->enum('blog_category',['news','education','entertainment','article'])->default("news");
            $table->enum('blog_status',['active','inactive'])->default("inactive");
            $table->string('blog_image')->nullable();
            $table->foreignId('added_by')->nullable()->constrained('users','id')->cascadeOnUpdate()->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
