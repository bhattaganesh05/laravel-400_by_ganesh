<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = array(
            array(
                'category_title' => 'News',
                'slug' => \Str::slug('News'),
                'category_status' => 'active'
            ),
            array(
                'category_title' => 'Education',
                'slug' => \Str::slug('Education'),
                'category_status' => 'active'
            ),
            array(
                'category_title' => 'Article',
                'slug' => \Str::slug('Article'),
                'category_status' => 'active'
            ),
            array(
                'category_title' => 'Entertainment',
                'slug' => \Str::slug('Entertainment'),
                'category_status' => 'active'
            )
        );
        DB::table('categories')->insert($category);
    }
}
