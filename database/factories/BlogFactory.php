<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    $blog_status = ['active','inactive'];
    return [
        'blog_title' => $faker->sentence(3),
        'blog_summary' => $faker->sentence(6),
        'blog_description' => $faker->sentence(12),
        'slug' => \Str::slug($faker->sentence(3)),
        'category_id' => random_int(1,4),
        'blog_status' => $blog_status[random_int(0,count($blog_status)-1)],
        'blog_image' => "default_image.jpg",
    ];
});
