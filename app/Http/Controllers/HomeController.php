<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // return view('home');
        return redirect()->route(request()->user()->role);
    }
    public function admin()
    {
        if(request()->user()->role != "admin"){
            return redirect()->route(request()->user()->role);
        }
        return view('home');
    }
    public function user()
    {
        if(request()->user()->role != "user"){
            return redirect()->route(request()->user()->role);
        }
        return view('home');
    }
}
