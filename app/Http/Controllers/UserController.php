<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public  function  about(){
        $user_data = array(
            array(
                "name" => 'Ganesh',
                "phone" => '9848979669',
                "email" => 'bhattaganesh05@gmail.com',
                "address" => 'Kailali'
            ),
            array(
                "name" => 'Akon',
                "phone" => '98489476325',
                "email" => 'gp05@gmail.com',
                "address" => 'dhangadhi'
            )
        );
        return view('about')->with("user_data" , $user_data);
    }
}
