<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function GuzzleHttp\Promise\all;
use App\Models\Category;
use App\Models\Blog;
use App\User;
use App\Mail\notify;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;
use Mail;

$blog = new Blog();
class BlogController extends Controller
{
    protected $blog = null;
    public function __construct(Blog $blog){
        $this->blog = $blog;
        $this->middleware(['auth'])->except(['getAllBlogs']);
    }
    public function getAllBlogs(){
        return response()->json(
            [
                'data'=>$this->blog->get(),
                'status'=>true,
                'msg'=>'success'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
/*        $blog_data = array(
            array(
                "title" => 'Programmingpot',
                "summary" => 'This is a blog programming blog',
                "description" => 'This provides programming tutorials on front-end as well as backend.',
                "category" => 'Education',
                "status" => 'Published'
            ),
            array(
                "title" => 'Nepali Jokes',
                "summary" => 'This is a blog for entertainment',
                "description" => 'This provides funny jokes in nepali, hindi and english langauge.',
                "category" => 'Entertainment',
                "status" => 'Un-published'
            )
        );*/
//        $blog_data = $this->blog->paginate(5);
        $blog_data = $this->blog->latest()->get();
        return view('blog.index')->with("blog_data" , $blog_data)->with("title","Home Page");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category();
//      $all_cat = $category->get();
        $all_cat = $category->pluck('category_title','id');//(value,key)

//        dd($all_cat);
        return view("blog.blog_form_using_lc")->with("title","Blog Create Form")
            ->with("all_cat",$all_cat);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*
        dd($request);
        dd($request->only(['blog_summary','blog_description','blog_title']));
        dd($request->except(['blog_summary','blog_description','blog_title']));
          dd($request->input('blog_title'));
            dd($request->blog_title);
               $request->request->add(["is_featured"=>"true"]);
               $request->request->add(["blog_title" => "ksllaskfjasldjf"]);
                dd($request);
        */
        $rules = $request->validate($this->blog->getRules());
        $data = $request->all();
        $data['slug'] = \Str::slug($request->blog_title);
        if($request->hasFile('blog_image')){
            $extension = $request->file('blog_image')->getClientOriginalExtension();
            $folder_name = "blog_image".date('ymdhis').rand(0,999);
            $image = $request->file('blog_image')->storeAs($folder_name,"blob_image.".$extension,'public');
            $data['blog_image'] = $image;
            $path = storage_path('app/public/').$data['blog_image'];
            $thumb_success = Image::make($path)->resize(300,300,function ($constraint){
                return $constraint->aspectRatio();
            })->save($path);

        }
//        dd($data);
//        $inserted_data = $this->blog->create($data);
//        dd($inserted_data);
        $this->blog->fill($data);
    // dd($this->blog);
        $status = $this->blog->save();
        if($status){
            $user =  new User;
            $all_users = $user->get();
            foreach ($all_users as $all_user){
                Mail::to($all_user->email)->send(new notify($this->blog,$all_user->name));
            }
            return redirect()->route('blog.index')->with('success',"Blog inserted successfully");

        }else{
            return redirect()->route('blog.create')->with('error',"Soryy!, error while  inserting blog ");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_data = $this->blog->findOrFail($id);
        $category = new Category();
//        $all_cat = $category->get();
        $all_cat = $category->pluck('category_title','id');//(value,key)
//        dd($all_cat);
//        dd($edit_data);
//        return view('blog.blog_form',compact('$edit_data'));
        return view('blog.blog_form_using_lc')->with("edit_blog_data" , $edit_data)->with("title","Blog Edit form")
            ->with('all_cat',$all_cat);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = $request->validate($this->blog->getRules());
        $data = $request->all();
        $image_path = explode('/',$data['folder_name']);
            if(count($image_path)>1){
                $folder_name = $image_path[0];
            }else{
                $folder_name = "blog_image".date('ymdhis').rand(0,999);
            }
        $data['slug'] = \Str::slug($request->blog_title);
        if($request->hasFile('blog_image')){
            $extension = $request->file('blog_image')->getClientOriginalExtension();
            $image = $request->file('blog_image')->storeAs($folder_name,"blob_image.".$extension,'public');
            $data['blog_image'] = $image;
            $path = storage_path('app/public/').$data['blog_image'];
            $thumb_success = Image::make($path);
            $thumb_success->resize(300,300,function ($constraint){ return $constraint->aspectRatio();});
            $thumb_success->save($path);
        }
        $status = Blog::find($id)->update($data);;
        if($status){
            return redirect()->route('blog.index')->with('success',"Blog updated successfully");
        }else{
            return redirect()->route('blog.index')->with('error',"Sorry!, error while  updating blog ");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $data = Blog::find($id);
        $file_check = $data['blog_image'];
        $file_check = explode('/',$file_check);
        $deleted = Blog::find($id)->delete();
        if(!$deleted){
            return redirect()->route('blog.index')->with('error',"Soryy!, error while  deleting blog");

        }else{
            if(count($file_check)>1){
                $path = storage_path('app/public/').$data->blog_image;
                if(File::exists($path)){
                    unlink($path);
                    rmdir(storage_path('app/public/'.$file_check[0]));
                }
            }
            return redirect()->route('blog.index')->with('success',"Blog deleted successfully");

        }

    }
    public function test($path){
        return Storage::url($path);
    }

}
