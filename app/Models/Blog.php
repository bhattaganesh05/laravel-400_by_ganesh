<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Blog extends Model
{
    protected  $fillable = ['blog_title','blog_summary','slug','category_id','blog_description','blog_status','blog_image','added_by'];

    public function category_info(){
        return $this->hasOne('App\Models\Category','id','category_id');

    }
    public function  getRules(){
        return ([
            'blog_title' => ['required','string','max:100'],
            'category_id' => ['required','exists:categories,id'],
            'blog_summary' => ['required','string'],
            'blog_description' => "nullable|string",
            'blog_status' => "required|in:active,inactive",
//            'blog_image' => "sometimes|image|max:5000"
        ]);
    }

}


