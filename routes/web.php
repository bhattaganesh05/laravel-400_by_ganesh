<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/*Route::get('/about', function (){
  $user_data = array(
      array(
          "name" => 'Ganesh',
          "phone" => '9848979669',
          "email" => 'bhattaganesh05@gmail.com',
          "address" => 'Kailali'
      ),
      array(
          "name" => 'Akon',
          "phone" => '98489476325',
          "email" => 'gp05@gmail.com',
          "address" => 'dhangadhi'
      )
  );
  return view('about')
      ->with("user_data" , $user_data)
      ->with("title","about Us Page");
})->name('about');*/
Route::get('/about',"UserController@about")->name("user.about");
Route::resource('/blog',"BlogController");
Route::get('/blog/test',"BlogController@test")->name('blog.test');

Route::get('/invoke','InvokableController');
Route::group(['prefix'=>'admin'],function (){
    Route::get('/about', function (){echo "this is about page";});
    Route::get('/join', function (){echo "this is join page";});
    Route::get('/index', function (){echo "this is index page";});
});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix'=>'/user','middleware'=>['auth','user']],function(){
Route::get('/', 'HomeController@user')->name('user');
});
Route::group(['prefix'=>'/admin','middleware'=>['auth','admin']],function(){
Route::get('/', 'HomeController@admin')->name('admin');
});
